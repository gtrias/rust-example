FROM rust

RUN apt update && apt install -y libclang-dev clang

WORKDIR /app
COPY . .

RUN cargo install --path .

CMD ["hello_world"]
